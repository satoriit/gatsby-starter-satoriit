require("./src/styles/index.scss")
require("./src/styles/bootstrap.scss")

// without the below the position is not reset when navigating to new page
// eslint-disable-next-line import/prefer-default-export
export const onRouteUpdate = () => {
  if (typeof window !== "undefined") {
    window.scrollTo(0, 0)
  }
}

// export { default as wrapRootElement } from './src/state/ReduxWrapper';
