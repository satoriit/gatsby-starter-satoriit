FROM satoridev/node-15-jenkins-build:gatsby-cli-3-11 as build

WORKDIR /app
ENV NODE_ENV=production
COPY . ./
RUN yarn install --frozen-lockfile
ADD "https://www.random.org/cgi-bin/randbyte?nbytes=10&format=h" skipcache
#RUN sed -i 's/error\.code[[:space:]]===[[:space:]]99/error.exitCode===99/' /app/node_modules/imagemin-pngquant/index.js
#RUN yarn add gatsby-plugin-sharp@png-revert --registry=https://registry.wardpeet.dev
RUN gatsby build --verbose

FROM nginx:1.15.4-alpine

COPY --from=build /app/public /usr/share/nginx/html
COPY --from=build /app/nginx.out.conf /etc/nginx/nginx.conf
RUN cat /etc/nginx/nginx.conf
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
