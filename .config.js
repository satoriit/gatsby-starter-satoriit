/**
 * @type {{wordPressUrl: string}}
 */
require("dotenv").config()

const config = {
  wordPressUrl: `${process.env.WORDPRESS_URL}`,
  siteUrl: `http://localhost:8000/`
}

module.exports = config
