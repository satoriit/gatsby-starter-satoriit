/**
 * Configure your Gatsby site with this file.
 *
 * See: https://www.gatsbyjs.org/docs/gatsby-config/
 */
require("dotenv").config({
  path: `.env.${process.env.NODE_ENV}`
})

// const config = require("./config")
module.exports = {
  siteMetadata: {
    title: process.env.ORG_NAME,
    description: process.env.ORG_NAME,
    author: process.env.ORG_NAME,
    siteUrl: `${process.env.SITE_URL}`
  },
  flags: { FAST_DEV: true },
  /* Your site config here */
  plugins: [
    // {
    //   resolve: `gatsby-plugin-google-analytics`,
    //   options: {
    //     // The property ID; the tracking code won't be generated without it
    //     trackingId: "INSERT_ID",
    //     // Defines where to place the tracking script - `true` in the head and `false` in the body
    //     head: true,
    //     // Avoids sending pageview hits from custom paths
    //     exclude: ["/preview/**", "/do-not-track/me/too/"],
    //     // Delays sending pageview hits on route update (in milliseconds)
    //     pageTransitionDelay: 0
    //   }
    // },

    // optional: if you are using path prefix, see plugin option below
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: process.env.ORG_NAME,
        short_name: process.env.ORG_SHORT_NAME,
        start_url: `/`,
        background_color: `#0d1927`,
        theme_color: `#0d1927`,
        // Enables "Add to Homescreen" prompt and disables browser UI (including back button)
        // see https://developers.google.com/web/fundamentals/web-app-manifest/#display
        display: `standalone`,
        icon: `src/images/favicon.svg` // This path is relative to the root of the site.
      }
    },
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-sass`,
    `gatsby-plugin-sitemap`,
    `gatsby-plugin-image`,
    `gatsby-transformer-sharp`,
    {
      resolve: "gatsby-plugin-sharp",
      options: { failOnError: false }
    },
    {
      resolve: "gatsby-plugin-robots-txt",
      options: {
        host: process.env.SITE_HOST,
        sitemap: `${process.env.SITE_HOST}/sitemap.xml`,
        env: {
          development: {
            policy: [{ userAgent: "*", disallow: ["/"] }]
          },
          production: {
            policy: [{ userAgent: "*", allow: "/" }]
          }
        }
      }
    },
    {
      resolve: `gatsby-plugin-canonical-urls`,
      options: {
        siteUrl: `${process.env.SITE_URL}`
      }
    },
    // {
    //   resolve: "gatsby-plugin-google-tagmanager",
    //   options: {
    //     id: "",

    //     // Include GTM in development.
    //     //
    //     // Defaults to false meaning GTM will only be loaded in production.
    //     includeInDevelopment: false,

    //     // datalayer to be set before GTM is loaded
    //     // should be an object or a function that is executed in the browser
    //     //
    //     // Defaults to null
    //     defaultDataLayer: { platform: "gatsby" },

    //     // Name of the event that is triggered
    //     // on every Gatsby route change.
    //     //
    //     // Defaults to gatsby-route-change
    //     // routeChangeEventName: "YOUR_ROUTE_CHANGE_EVENT_NAME",
    //   },
    // },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `${__dirname}/src/images`,
        name: "images"
      }
    },
    {
      resolve: "gatsby-source-wordpress",
      options: {
        url: `${process.env.WORDPRESS_URL}/graphql`,
        // Use 'Advanced Custom Fields' Wordpress plugin
        useACF: true,
        schema: {
          perPage: 50
          // requestConcurrency: 5,
          // previewRequestConcurrency: 2
        }
      }
    },
    {
      resolve: "gatsby-plugin-react-svg",
      options: {
        rule: {
          include: /\.inline\.svg$/
        }
      }
    },
    {
      resolve: `gatsby-plugin-portal`,
      options: {
        key: "portal",
        id: "portal-root"
      }
    },
    {
      resolve: `gatsby-plugin-breadcrumb`,
      options: {
        // defaultCrumb: optional To create a default crumb
        // see Click Tracking default crumb example below
        // defaultCrumb: {
        //   location: {
        //     pathname: "/"
        //   },
        //   crumbLabel: "Home",
        //   crumbSeparator: " / "
        // }
        useAutoGen: true,
        exclude: [
          `**/dev-404-page/**`,
          `**/404/**`,
          `**/404.html`,
          `**/offline-plugin-app-shell-fallback/**`
        ],
        excludeOptions: {
          separator: "."
        }
        // crumbLabelUpdates: [
        //   {
        //     pathname: '/news',
        //     crumbLabel: 'Books'
        //   }
        // ],
      }
    }
    // See: https://satori.myjetbrains.com/youtrack/articles/SIT-A-24/Intergrations
    // {
    //   resolve: `gatsby-source-eventbrite`,
    //   options: {
    //     organizationId: `INSERT_ID`,
    //     accessToken: `INSERT_TOKEN`,
    //     entities: ["events"]
    //   }
    // }
  ]
}
