/* eslint-disable no-console */
const path = require(`path`)

module.exports = async ({ actions: { createPage }, graphql }) => {
  const pageTemplate = path.resolve(`./src/templates/page.js`)
  const postTemplate = path.resolve(`./src/templates/post.js`)

  const { data } = await graphql(`
    query GET_PAGES_DATA {
      allWpPage {
        nodes {
          id
          uri
          flexLayouts {
            pageLayouts {
              ... on WpPage_Flexlayouts_PageLayouts_PostsFeed {
                fieldGroupName
              }
            }
          }
        }
      }
      allWpPost {
        nodes {
          uri
          date(formatString: "DD MMM YYYY", locale: "en-GB")
          title
          id
          slug
          link
          categories {
            nodes {
              name
            }
          }
          flexLayouts {
            pageLayouts {
              ... on WpPost_Flexlayouts_PageLayouts_PostHeader {
                postTitle
                blurb
                image {
                  altText
                  sourceUrl
                  localFile {
                    childImageSharp {
                      gatsbyImageData(
                        formats: [AUTO, WEBP, AVIF]
                        width: 1032
                        placeholder: BLURRED
                      )
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  `)

  await Promise.all(
    data.allWpPage.nodes.map(async (page, i) => {
      if (i === 0) {
        console.log(`\n-------\n\nCreating pages\n\n---\n`)
      }

      const { uri } = page
      const pagePath = uri === "/home/" ? `/` : uri
      const hasFeed = page.flexLayouts.pageLayouts?.filter(
        item => item.fieldGroupName === "Page_Flexlayouts_PageLayouts_PostsFeed"
      )

      if (hasFeed?.length) {
        console.log(`Creating page ${i + 1}: ${uri}`)

        await createPage({
          path: pagePath,
          component: pageTemplate,
          context: { id: page.id, page, allPosts: data.allWpPost.nodes }
        })
      } else {
        console.log(`Creating page ${i + 1}: ${uri}`)

        await createPage({
          path: pagePath,
          component: pageTemplate,
          context: page
        })
      }
    }),

    data.allWpPost.nodes.map(async (post, i) => {
      if (i === 0) {
        console.log(`\n-------\n\nCreating posts\n\n---\n`)
      }

      const { uri } = post

      console.log(`Creating post ${i + 1}: ${uri}`)

      await createPage({
        path: uri,
        component: postTemplate,
        context: post
      })
    })
  )
}
