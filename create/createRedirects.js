/* eslint-disable no-console */
module.exports = async ({ actions: { createRedirect }, graphql }) => {
  const GET_REDIRECTIONS = `
    query GET_REDIRECTIONS($id: String!){
      wpRedirect(id: {eq: $id}) {
          redirection {
            redirections
          }
        }
      }
    `

  const fetchRedirects = async variables =>
    // eslint-disable-next-line no-return-await
    await graphql(GET_REDIRECTIONS, variables).then(({ data }) => {
      const {
        wpRedirect: {
          redirection: { redirections }
        }
      } = data

      return redirections
    })

  await fetchRedirects({ id: "cG9zdDozMzM=" }).then(redirections => {
    console.log("Redirections", redirections)
    if (redirections != null) {
      redirections.split("\r\n").forEach(redirection => {
        const redirect = redirection.split(" ")
        console.log(
          "redirections broken down:\nredirect[0]:",
          redirect[0],
          "\nredirect[1]:",
          redirect[1]
        )
        createRedirect({
          fromPath: redirect[0],
          toPath: redirect[1],
          isPermanent: true
        })
      })
    }
  })
}
