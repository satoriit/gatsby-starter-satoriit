/* eslint-disable no-underscore-dangle */
/* eslint-disable no-console */
import { remove } from "fs-extra"
import { get } from "lodash"
import { NginxConfFile } from "nginx-conf"

const path = require(`path`)
const { createRemoteFileNode } = require("gatsby-source-filesystem")

/** TODO: Cannot read property 'redirection'
 of null
 */

const createRedirects = require(`./create/createRedirects`)

const createSitePages = require(`./create/createSitePages`)

exports.createPages = async ({ actions, graphql }) => {
  await createRedirects({ actions, graphql })
  await createSitePages({ actions, graphql })
}

/**
 * Download WordPress images, add them to GraphQL schema.
 * @link https://www.gatsbyjs.org/docs/node-apis/#createResolvers
 * @link https://www.gatsbyjs.org/packages/gatsby-source-filesystem/?=#createremotefilenode
 */
exports.createResolvers = ({
  actions,
  cache,
  createNodeId,
  createResolvers,
  store,
  reporter
}) => {
  const { createNode } = actions
  createResolvers({
    wpMediaItem: {
      localFile: {
        type: `File`,
        resolve(source) {
          return createRemoteFileNode({
            url: source.url, // TODO: CONFIRM
            store,
            cache,
            createNode,
            createNodeId,
            reporter
          })
        }
      }
    }
  })
}

/*
 * added to clear chunking mini-css-extract-plugin error
 * Solution taken from https://stackoverflow.com/q/67474408/7794529
 */
exports.onCreateWebpackConfig = ({ stage, actions, getConfig }) => {
  actions.setWebpackConfig({
    resolve: {
      modules: ["node_modules", path.resolve(__dirname, "src")],
      alias: {
        "basic-info": path.resolve(__dirname, "src/app/routes/basic-info"),
        "situation-analysis": path.resolve(
          __dirname,
          "src/app/routes/situation-analysis"
        ),
        "select-drivers": path.resolve(
          __dirname,
          "src/app/routes/select-drivers"
        ),
        "define-vision": path.resolve(
          __dirname,
          "src/app/routes/define-vision"
        ),
        "rate-drivers": path.resolve(__dirname, "src/app/routes/rate-drivers"),
        "affinity-groups": path.resolve(
          __dirname,
          "src/app/routes/affinity-groups"
        ),
        "define-objectives": path.resolve(
          __dirname,
          "src/app/routes/define-objectives"
        ),
        "create-roadmap": path.resolve(
          __dirname,
          "src/app/routes/create-roadmap"
        )
      }
    },

    devtool: "eval-source-map"
  })

  if (stage === "build-javascript" || stage === "develop") {
    const config = getConfig()

    const miniCssExtractPlugin = config.plugins.find(
      plugin => plugin.constructor.name === "MiniCssExtractPlugin"
    )

    if (miniCssExtractPlugin) miniCssExtractPlugin.options.ignoreOrder = true

    actions.replaceWebpackConfig(config)
  }
}

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms))
}

function isValidUrl(string) {
  try {
    // eslint-disable-next-line no-new
    new URL(string)
  } catch (_) {
    return false
  }

  return true
}

console.log("env=", process.env.NODE_ENV)
// eslint-disable-next-line func-names
exports.onPostBuild = async function (
  { store, reporter },
  {
    outputConfigFile = `${__dirname}/nginx.out.conf`,
    inputConfigFile = `${__dirname}/nginx.conf`,
    whereToIncludeRedirects = "http.server"
  }
) {
  // https://github.com/gimoteco/gatsby-plugin-nginx-redirect/blob/master/src/gatsby-node.js
  console.log("Configuring redirections")
  const { redirects } = store.getState()
  await remove(outputConfigFile)

  return new Promise(resolve => {
    NginxConfFile.create(inputConfigFile, async (err, conf) => {
      if (err) {
        console.log(err)
        return
      }

      conf.die(inputConfigFile)
      conf.flush()
      await sleep(500)

      console.log("for each redirects", redirects)
      redirects.forEach(redirect => {
        if (redirect.toPath.includes("http")) {
          if (isValidUrl(redirect.toPath)) {
            get(conf.nginx, whereToIncludeRedirects, conf.nginx)._add(
              "rewrite",
              `^${redirect.fromPath}\\/?$ ${redirect.toPath} permanent`
            )
          }
        } else {
          get(conf.nginx, whereToIncludeRedirects, conf.nginx)._add(
            "rewrite",
            `^${redirect.fromPath}\\/?$ https://$host${redirect.toPath} ${
              redirect.isPermanent ? "permanent" : "redirect"
            }`
          )
        }
      })

      conf.live(outputConfigFile)
      conf.flush()
      await sleep(500)
      resolve()
    })

    reporter.warn(`Added redirects to ${outputConfigFile}`)
  })
}
