import React from "react"
import { Container, Row, Col } from "react-bootstrap"

import { Layout } from "../components/Layout"
import ErrorImg from "../components/UI/svg/NotFound"

import { titleContainerClass } from "./404.module.scss"

const PageNotFound = pageData => (
  <Layout
    seoComponent={{ seoTitle: "404 error", seoDescription: "Page not found" }}
    location={{ pathname: "page-not-found" }}>
    <section className="py-6 py-md-9">
      <Container>
        <Row className="d-flex justify-content-sm-center pt-lg-5 ">
          <Col
            xs={12}
            lg={5}
            className="d-flex flex-column justify-content-start justify-content-lg-center align-items-center mb-5 mb-lg-0">
            <ErrorImg />
          </Col>
          <Col xs={12} lg={7}>
            <div className={titleContainerClass}>
              <h1>Page not found!</h1>
              <p className="lead">Ooops!</p>
            </div>
          </Col>
        </Row>
      </Container>
    </section>
  </Layout>
)

export default PageNotFound
