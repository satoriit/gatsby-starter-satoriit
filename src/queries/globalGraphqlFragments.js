/* eslint-disable import/prefer-default-export */
import { graphql } from "gatsby"

export const query = graphql`
  fragment GatsbyImageFragment on WpMediaItem {
    altText
    sourceUrl
    localFile {
      childImageSharp {
        gatsbyImageData(formats: [AUTO, WEBP, AVIF], placeholder: BLURRED)
      }
    }
  }
  fragment GatsbyFixedImageFragment on WpMediaItem {
    altText
    sourceUrl
    localFile {
      childImageSharp {
        gatsbyImageData(
          layout: FIXED
          formats: [AUTO, WEBP, AVIF]
          placeholder: BLURRED
        )
      }
    }
  }
  fragment FullWidthImageFragment on WpMediaItem {
    altText
    sourceUrl
    localFile {
      childImageSharp {
        gatsbyImageData(
          formats: [AUTO, WEBP, AVIF]
          width: 1920
          placeholder: BLURRED
        )
      }
    }
  }
  fragment MenuFieldsFragment on WpMenuItem {
    id
    label
    url
    parentId
  }
  fragment EventsFragment on EventbriteEvents {
    id
    name {
      text
    }
    summary
    url
    source
    listed
    start {
      local(formatString: "DD MMM")
    }
    status
    logo {
      original {
        localFile {
          childImageSharp {
            gatsbyImageData(
              formats: [AUTO, WEBP, AVIF]
              width: 100
              placeholder: BLURRED
            )
          }
        }
      }
    }
  }
`
