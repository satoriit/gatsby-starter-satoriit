import { graphql, useStaticQuery } from "gatsby"
import { StaticImage } from "gatsby-plugin-image"
import React, { Fragment, useRef, useState } from "react"
import { Navbar, Nav, NavDropdown } from "react-bootstrap"

import hierarchizeFlatList from "../../../utils/hierarchizeFlatList"
import Link from "../../UI/Link"
import { ExternalLink } from "../../UI/svg"
import Close from "../../UI/svg/Close"
import Hamburger from "../../UI/svg/Hamburger"

import {
  navbarClass,
  navLinkClass,
  openMobileNavClass,
  activePrimaryNavLinkClass,
  primaryNavLinkClass,
  dropdownNavLinkClass,
  topNavbarClass,
  currentNavLinkClass
} from "./NavbarContainer.module.scss"

const MENU_QUERY = graphql`
  query GET_MOBIL_MENU_ITEMS {
    allWpMenuItem(
      filter: { locations: { in: PRIMARY } }
      sort: { fields: order, order: ASC }
    ) {
      nodes {
        ...MenuFieldsFragment
      }
    }
  }
`

const renderNavLink = (menuItem, location) => {
  const isCurrentLink = !!(
    (menuItem.url === "/home/" && location.pathname === "/") ||
    menuItem.url === location.pathname
  )

  return (
    <Link
      className={`
        ${isCurrentLink ? `${currentNavLinkClass}` : ""}
        ${navLinkClass}
      `}
      link={menuItem.url}
      title={menuItem.label}
    />
  )
}

const renderDropDownNavLink = menuItem => (
  <Link
    className={dropdownNavLinkClass}
    link={menuItem.url}
    title={menuItem.label}
  />
)

const RenderDropdown = (menuItem, dropdownMenuCount, index, location) => {
  const dropdownRef = useRef()
  const [status, setStatus] = useState(
    Array(dropdownMenuCount)
      .fill()
      .map(x => false)
  )

  // is the link the current page open on the browser
  const isCurrentLink =
    menuItem.children.filter(item => item.url === location.pathname).length > 0

  const updateStatus = (value, i) => {
    const clone = [...status]
    clone[i] = value
    setStatus(clone)
  }

  return (
    <NavDropdown
      className={`${primaryNavLinkClass}
      ${isCurrentLink ? `${currentNavLinkClass}` : ""}
      ${status[index] ? `${activePrimaryNavLinkClass} active` : ""}
      `}
      ref={dropdownRef}
      id={menuItem.id}
      title={menuItem.label}
      onClick={e => updateStatus(!status[index], index)}
      show={status[index]}>
      {menuItem.children.map(item => renderDropDownNavLink(item))}
    </NavDropdown>
  )
}

const MobileMenu = ({ location, toggle }) => {
  const [mobileToggle, setMobileToggle] = toggle

  const data = useStaticQuery(MENU_QUERY)
  const hierarchicalData = hierarchizeFlatList(data.allWpMenuItem.nodes)
  const dropdownMenuCount = hierarchicalData.filter(
    item => item.children.length > 0
  ).length

  return (
    <>
      <Navbar
        className={`${navbarClass} mobile-navbar`}
        bg="white"
        fixed="top"
        expand="lg">
        <Link className="lh-1" link="/">
          <StaticImage
            src="../../../images/tm-logo.png"
            alt={`${process.env.ORG_NAME} Logo`}
            width={100}
          />
        </Link>

        <Navbar.Toggle
          // className={navbarToggleClass}
          aria-controls="basic-navbar-nav"
          onClick={() => setMobileToggle(!mobileToggle)}>
          {mobileToggle ? <Close /> : <Hamburger />}
        </Navbar.Toggle>

        <Navbar.Collapse className={openMobileNavClass} id="basic-navbar-nav">
          <Nav>
            {hierarchicalData.map((menuItem, i) => {
              const index = i
              return (
                <Fragment key={menuItem.id}>
                  {menuItem.children.length
                    ? RenderDropdown(
                        menuItem,
                        dropdownMenuCount,
                        index,
                        location
                      )
                    : renderNavLink(menuItem, location)}
                </Fragment>
              )
            })}
          </Nav>
          <Nav className={topNavbarClass}>
            <Nav.Item>
              <a className="p-0" href={process.env.DONATE_LINK}>
                Donate <ExternalLink className="ms-1" size={16} />
              </a>
            </Nav.Item>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    </>
  )
}

export default MobileMenu
