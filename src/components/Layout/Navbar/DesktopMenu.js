import { graphql, navigate, useStaticQuery } from "gatsby"
import { StaticImage } from "gatsby-plugin-image"
import React, { useState, useRef, Fragment } from "react"
import { Col, Nav, Navbar, NavDropdown, Row } from "react-bootstrap"
import { v4 as uuidv4 } from "uuid"

import hierarchizeFlatList from "../../../utils/hierarchizeFlatList"
import Link from "../../UI/Link"
import { ExternalLink } from "../../UI/svg"

import {
  navbarClass,
  navLinkClass,
  topNavbarClass,
  primaryNavLinkClass,
  activePrimaryNavLinkClass,
  dropdownNavLinkClass,
  leftPositionClass,
  rightPositionClass,
  currentNavLinkClass
} from "./NavbarContainer.module.scss"

const MENU_QUERY = graphql`
  query GET_MENU_ITEMS {
    allWpMenuItem(
      filter: { locations: { in: PRIMARY } }
      sort: { fields: order, order: ASC }
    ) {
      nodes {
        ...MenuFieldsFragment
      }
    }
  }
`

const renderNavLink = (menuItem, location) => {
  const isCurrentLink = !!(
    (menuItem.url === "/home/" && location.pathname === "/") ||
    menuItem.url === location.pathname
  )

  return (
    <Link
      className={`
      ${navLinkClass} nav-link
      ${isCurrentLink ? `${currentNavLinkClass}` : ""}
      `}
      link={menuItem.url}
      title={menuItem.label}
    />
  )
}

const renderDropDownNavLink = menuItem => (
  <Link
    className={`${dropdownNavLinkClass} nav-link`}
    link={menuItem.url}
    title={menuItem.label}
  />
)

const RenderDropdown = (
  menuItem,
  dropdownMenuCount,
  navItemCount,
  index,
  location
) => {
  const firstHalfCount = Math.floor(navItemCount / 2)

  let dropdownPositionClass = ""
  if (navItemCount <= 4) {
    dropdownPositionClass = rightPositionClass
  } else {
    dropdownPositionClass =
      index <= firstHalfCount ? leftPositionClass : rightPositionClass
  }

  // is the link the current page open on the browser
  const isCurrentLink =
    menuItem.children.filter(item => item.url === location.pathname).length > 0

  const dropdownRef = useRef()
  const [status, setStatus] = useState(
    Array(dropdownMenuCount)
      .fill()
      .map(x => false)
  )

  const updateStatus = (value, i) => {
    const clone = [...status]
    clone[i] = value
    setStatus(clone)
  }

  const onMouseClick = e => {
    // updateStatus(true, index)
    // only navigate if menu item is the primary dropdown
    if (!e.target.className.includes("dropdownNavLinkClass")) {
      e.preventDefault()
      navigate(menuItem.url)
    }
  }

  return (
    <NavDropdown
      className={`${primaryNavLinkClass}
        ${isCurrentLink ? `${currentNavLinkClass}` : ""}
        ${
          status[index]
            ? `${activePrimaryNavLinkClass} ${dropdownPositionClass} active`
            : ""
        }
        `}
      ref={dropdownRef}
      id={menuItem.id}
      title={menuItem.label}
      onClick={e => onMouseClick(e)}
      onMouseEnter={() => updateStatus(true, index)}
      onMouseLeave={() => updateStatus(false, index)}
      // onClick={() => updateStatus(true, index)}
      show={status[index]}>
      <Row xl="3" lg="2">
        {menuItem.children.map(item => (
          <Col key={uuidv4()}>{renderDropDownNavLink(item)}</Col>
        ))}
      </Row>
    </NavDropdown>
  )
}

const DesktopMenu = ({ location }) => {
  const data = useStaticQuery(MENU_QUERY)
  const hierarchicalData = hierarchizeFlatList(data.allWpMenuItem.nodes)
  const navItemCount = hierarchicalData.length
  const dropdownMenuCount = hierarchicalData.filter(
    item => item.children.length > 0
  ).length

  return (
    <>
      <Nav className={`${topNavbarClass}`} aria-label="Call to action">
        <div>
          <Nav.Item className="p-4">
            <a className="p-0" href={process.env.DONATE_LINK}>
              Donate <ExternalLink className="ms-1" size={16} />
            </a>
          </Nav.Item>
        </div>
      </Nav>

      <Navbar bg="white" aria-label="Top Logo">
        <div className="d-flex align-items-center ms-3">
          <Link className="lh-1" link="/">
            <StaticImage
              src="../../../images/tm-logo.png"
              alt={`${process.env.ORG_NAME} Logo`}
              width={180}
            />
          </Link>
        </div>
      </Navbar>
      <Navbar className={`${navbarClass}`} expand="lg" aria-label="Main">
        <Navbar.Collapse className="flex-grow-0" id="basic-navbar-nav">
          <Nav className="d-flex justify-content-between align-items-center">
            {hierarchicalData.map((menuItem, i) => {
              const index = i

              return (
                <Fragment key={menuItem.id}>
                  {menuItem.children.length
                    ? RenderDropdown(
                        menuItem,
                        dropdownMenuCount,
                        navItemCount,
                        index,
                        location
                      )
                    : renderNavLink(menuItem, location)}
                </Fragment>
              )
            })}
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    </>
  )
}

export default DesktopMenu
