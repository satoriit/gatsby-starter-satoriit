import React, { useState, useEffect } from "react"

import useMediaQuery from "../../../hooks/useMediaQuery"
import useScrollPosition from "../../../hooks/useScrollPosition"

import DesktopMenu from "./DesktopMenu"
import MobileMenu from "./MobileMenu"

const NavbarContainer = ({ location }) => {
  const [hideOnScroll, setHideOnScroll] = useState(false)
  const [mobileToggle, setMobileToggle] = useState(false)
  const [isMobile, setIsMobile] = useState(false)

  const mobileQuery = useMediaQuery("(max-width: 991px)")

  useEffect(() => {
    if (mobileQuery !== undefined) {
      setIsMobile(mobileQuery)
    }
  }, [mobileQuery])

  useScrollPosition(
    ({ prevPos, currPos }) => {
      const isShow = currPos.y > prevPos.y && currPos.y < -80
      if (isShow !== hideOnScroll) {
        setHideOnScroll(!hideOnScroll)
      }
    },
    [hideOnScroll],
    false,
    false,
    200
  )

  const renderNavbar = (fixed = false) => (
    <>
      {isMobile ? (
        <MobileMenu
          location={location}
          toggle={[mobileToggle, setMobileToggle]}
        />
      ) : (
        <>
          <DesktopMenu location={location} />
          {/* <FloatingActionButton /> */}
        </>
      )}
    </>
  )

  return <>{renderNavbar(hideOnScroll, location)}</>
}

NavbarContainer.defaultProps = {}

export default NavbarContainer
