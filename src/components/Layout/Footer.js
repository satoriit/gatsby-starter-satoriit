import { graphql, useStaticQuery } from "gatsby"
import { StaticImage } from "gatsby-plugin-image"
import { ceil } from "lodash"
import React, { /* Fragment */ useState, useEffect } from "react"
import { Navbar, Container, Row, Col } from "react-bootstrap"
// import { v4 as uuidv4 } from "uuid"

// import {
//   ShareTwitter as TwitterIcon,
//   ShareLinkedIn as LinkedInIcon,
//   ShareFacebook as FacebookIcon,
//   ShareEmail as EnvelopeIcon,
//   Trustpilot as TrustpilotLogo
// } from "../UI/svg"
import useMediaQuery from "../../hooks/useMediaQuery"
import hierarchizeFlatList from "../../utils/hierarchizeFlatList"
import Link from "../UI/Link"

const MENU_QUERY = graphql`
  query GET_FOOTER_MENU_ITEMS {
    allWpMenuItem(
      filter: { locations: { in: FOOTER } }
      sort: { fields: order, order: ASC }
    ) {
      nodes {
        ...MenuFieldsFragment
      }
    }
    #     wpMenu(locations: { eq: FOOTER }) {
    #     footerMenu {
    #       socialButtons {
    #         buttonType
    #         url
    #       }
    #     }
    #   }
  }
`

const Footer = () => {
  const data = useStaticQuery(MENU_QUERY)
  const hierarchicalData = hierarchizeFlatList(data.allWpMenuItem.nodes)

  const firstMenuColItems = hierarchicalData.slice(
    0,
    ceil(hierarchicalData.length / 2)
  )
  const secondMenuColItems = hierarchicalData.slice(
    ceil(hierarchicalData.length / 2)
  )

  // const { socialButtons } = data.wpMenu.footerMenu

  // const [isMobile, setIsMobile] = useState(false)

  const [isMobile, setIsMobile] = useState(false)

  const mobileQuery = useMediaQuery("(max-width: 605px)")

  useEffect(() => {
    if (mobileQuery !== undefined) {
      setIsMobile(mobileQuery)
    }
  }, [mobileQuery])

  return (
    <>
      <footer className="fw-bold">
        <Navbar className="px-3 px-xl-5" bg="secondary" aria-label="Footer">
          <Container className="d-block pt-6 pb-5">
            <div>
              <Row>
                <Col className="col-auto me-sm-8">
                  <ul className="mb-1 list-unstyled">
                    {firstMenuColItems.map(menuItem => (
                      <li key={menuItem.id} className="lh-lg">
                        <Link link={menuItem.url} title={menuItem.label} />
                      </li>
                    ))}
                  </ul>
                </Col>
                <Col className="col-auto">
                  <ul className="mb-1 list-unstyled">
                    {secondMenuColItems.map(menuItem => (
                      <li key={menuItem.id} className="lh-lg">
                        <Link link={menuItem.url} title={menuItem.label} />
                      </li>
                    ))}
                  </ul>
                </Col>

                <Col
                  xs={12}
                  md="auto"
                  className="d-flex flex-column mt-5 mt-md-0 ms-md-auto">
                  <p className="small">
                    {process.env.ORG_NAME}
                    <br />
                    {process.env.ORG_ADDRESS_LINE1}
                    <br />
                    {process.env.ORG_ADDRESS_LINE2}
                    <br />
                    {process.env.ORG_ADDRESS_LINE3}
                    <br />
                  </p>
                  {isMobile ? (
                    <a
                      href={`tel:${process.env.ORG_TEL}`}
                      className="mt-2 mb-0 small">
                      Tel {process.env.ORG_TEL}
                    </a>
                  ) : (
                    <p className="mt-2 mb-0 small">Tel {process.env.ORG_TEL}</p>
                  )}
                </Col>
              </Row>
            </div>
            {/* </div> */}
          </Container>
        </Navbar>

        <Navbar bg="white" className={` d-flex py-4 small`}>
          <Container className="justify-content-between" fluid="md">
            <div className="d-flex align-items-center">
              <Link className="lh-1" link="/">
                <>
                  {isMobile ? (
                    <StaticImage
                      src="../../images/tm-logo.png"
                      alt={`${process.env.ORG_NAME} Logo`}
                      width={69}
                    />
                  ) : (
                    <StaticImage
                      src="../../images/tm-logo.png"
                      alt={`${process.env.ORG_NAME} Logo`}
                      width={120}
                    />
                  )}
                </>
              </Link>
              <div className="ms-4">
                <div className="d-flex flex-column justify-content-between mx-0">
                  <p className="my-1 small">
                    {process.env.CHARITY_NUMBER &&
                      `Registered
              charity no. ${process.env.CHARITY_NUMBER}. `}
                    {process.env.COMPANY_NUMBER &&
                      `Company
                  limited by guarantee no. ${process.env.COMPANY_NUMBER}. `}
                  </p>
                </div>
                <div className="small">
                  © {new Date().getFullYear()} {process.env.ORG_NAME}. Website
                  by{" "}
                  <a
                    href="https://www.bravand.com/"
                    target="_blank"
                    rel="noopener noreferrer">
                    {" "}
                    Bravand
                  </a>
                </div>
              </div>
            </div>
            {/* {socialButtons && (
                    <div className={btnGroupClass}>
                      {socialButtons.map(button => {
                        const { buttonType, url } = button
                        return (
                          <Fragment key={uuidv4()}>
                            {buttonType === "Facebook" && (
                              <span className={shareBtnClass}>
                                <Link
                                  link={url}
                                  className="d-flex"
                                  resetButtonStyle>
                                  <FacebookIcon size={1.5} />
                                </Link>
                              </span>
                            )}

                            {buttonType === "Twitter" && (
                              <span className={shareBtnClass}>
                                <Link link={url} className="d-flex">
                                  <TwitterIcon size={1.5} />
                                </Link>
                              </span>
                            )}

                            {buttonType === "LinkedIn" && (
                              <span className={shareBtnClass}>
                                <Link link={url} className="share d-flex">
                                  <LinkedInIcon size={1.5} />
                                </Link>
                              </span>
                            )}

                            {buttonType === "Email" && (
                              <span className={shareBtnClass}>
                                <EmailShareButton
                                  subject={process.env.ORG_NAME}
                                  url={process.env.SITE_HOST}
                                  className="share d-flex">
                                  <EnvelopeIcon size={1.5} />
                                </EmailShareButton>
                              </span>
                            )}
                          </Fragment>
                        )
                      })}
                    </div>
                  )} */}
          </Container>
        </Navbar>
      </footer>
    </>
  )
}

export default Footer
