import he from "he"
import React from "react"
import Helmet from "react-helmet"

import CustomJavascript from "./CustomJavascript"
import Footer from "./Footer"
import Navbar from "./Navbar/NavbarContainer"

const TemplateWrapper = ({
  children,
  seoComponent,
  defaultMenuBackgroundColour,
  location
}) => {
  let title
  let description
  if (seoComponent && seoComponent.seoTitle) {
    title = he.decode(seoComponent.seoTitle)
  } else {
    // console.log("title not set", location)
    title = ""
  }

  if (seoComponent && seoComponent.seoDescription) {
    description = he.decode(seoComponent.seoDescription)
  }

  return (
    <div className="h-100">
      <Helmet
        encodeSpecialCharacters
        title={`${title} | ${process.env.ORG_SHORT_NAME}`}>
        {description && <meta name="description" content={description} />}
      </Helmet>
      <Navbar
        defaultMenuBackgroundColour={defaultMenuBackgroundColour}
        location={location}
      />
      <div>{children}</div>
      <Footer />
      <CustomJavascript />
    </div>
  )
}

export default TemplateWrapper
