import PropTypes from "prop-types"
import React from "react"
import { Container } from "react-bootstrap"

import * as vars from "../../styles/variableExports.module.scss"
import DecoratedLine from "../UI/DecoratedLine"
import SwirlBkgd from "../UI/svg/SwirlBkgd"

import {
  bkgdPatternContainerClass,
  containerClass,
  primaryBkgd,
  lightBkgd,
  sectionClass,
  bkgdPatternClass
} from "./Section.module.scss"

const Section = ({
  className,
  heading,
  children,
  variant,
  bkgdPattern,
  containerStyles,
  padding
}) => {
  let bkgdColor

  switch (variant) {
    case "primary":
      bkgdColor = primaryBkgd
      break
    case "light":
      bkgdColor = lightBkgd
      break
    case "light-pattern":
      bkgdColor = lightBkgd
      break
    default:
    // pass
  }

  const { title, className: titleClass } = heading || {}
  const { top, bottom } = padding || {}

  return (
    <section className={`${sectionClass} ${bkgdColor} ${className}`}>
      {bkgdPattern && (
        <div className={bkgdPatternContainerClass}>
          <SwirlBkgd
            className={bkgdPatternClass}
            color={variant === "primary" ? vars.primaryOffset : "white"}
          />
        </div>
      )}
      <Container
        className={`${containerClass} ${containerStyles || ""} ${top || ""} ${
          bottom || ""
        }`}>
        {title && (
          <>
            {" "}
            <h2 className={titleClass}>{title}</h2>
            <DecoratedLine />
          </>
        )}
        {children}
      </Container>
    </section>
  )
}

export default Section

Section.propTypes = {
  heading: PropTypes.shape({
    title: PropTypes.string,
    className: PropTypes.string
  }),
  children: PropTypes.element,
  variant: PropTypes.oneOf(["primary", "light", "default"]),
  bkgdPattern: PropTypes.bool
}

Section.defaultProps = {
  heading: {},
  variant: "default",
  children: null,
  bkgdPattern: false
}
