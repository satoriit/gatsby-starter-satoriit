import { graphql } from "gatsby"

export const fragment = graphql`
  fragment metadataSectionFragment on WpPage_Seo_SeoComponents_SeoComponents {
    seoTitle
    seoDescription
  }
`
export const postFragment = graphql`
  fragment postMetadataSectionFragment on WpPost_Seo_SeoComponents_SeoComponents {
    seoTitle
    seoDescription
  }
`

export const MetadataSection = ({ seoTitle, seoDescription }) => ({
  seoTitle,
  seoDescription
})
