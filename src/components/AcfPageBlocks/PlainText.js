import { graphql } from "gatsby"
import parseHTML from "html-react-parser"
import React from "react"
import { Row, Col } from "react-bootstrap"

import { cleanWYSIWYGtext } from "../../utils"
import Section from "../Layout/Section"

import { textBlockClass } from "./PlainText.module.scss"

export const fragment = graphql`
  fragment PlainTextPageFragment on WpPage_Flexlayouts_PageLayouts_PlainText {
    text
    padding {
      top
      bottom
    }
  }
  fragment PlainTextPostFragment on WpPost_Flexlayouts_PageLayouts_PlainText {
    text
    padding {
      top
      bottom
    }
  }
`

export const PlainText = ({ text, padding }) => {
  const content = cleanWYSIWYGtext(text)

  return (
    <Section padding={padding}>
      <Row>
        <Col lg={8}>
          <div className={textBlockClass}>{parseHTML(content)}</div>
        </Col>
      </Row>
    </Section>
  )
}
