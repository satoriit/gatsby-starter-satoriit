import { GatsbyImage } from "gatsby-plugin-image"
import React from "react"
import { Card as ReactBSCard } from "react-bootstrap"

import {
  imageWrapperClass,
  mainImageClass,
  postCardClass,
  standinImgWrapperClass,
  titleLinkClass
} from "./Card.module.scss"
import SwirlBkgd from "./svg/SwirlBkgd"

import { Link } from "."

const Card = ({ link, image, alt, date, title }) => (
  <ReactBSCard className={postCardClass}>
    <Link className={imageWrapperClass} link={link}>
      {image ? (
        <GatsbyImage className={mainImageClass} image={image} alt={alt} />
      ) : (
        <div className={standinImgWrapperClass}>
          <SwirlBkgd width={525} height={458} />
        </div>
      )}
    </Link>
    <ReactBSCard.Body className="d-flex flex-column justify-content-center px-6">
      <span className="mb-2 small fw-bold">{date}</span>
      <h3>
        <Link className={titleLinkClass} link={link}>
          {title}
        </Link>
      </h3>
    </ReactBSCard.Body>
  </ReactBSCard>
)

export default Card
