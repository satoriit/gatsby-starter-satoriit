import PropTypes from "prop-types"
import React from "react"

import {
  authorClass,
  blockquoteClass,
  quoteClass,
  textClass
} from "./PullQuote.module.scss"

const PullQuote = ({ quote, author }) => (
  <div className="d-flex justify-content-between">
    <div className={textClass}>
      <blockquote className={blockquoteClass}>
        <span className={quoteClass}>{quote}</span>
      </blockquote>
      {author && (
        <cite className={`${quoteClass} ${authorClass}`}>{`- ${author}`}</cite>
      )}
    </div>
  </div>
)

export default PullQuote

PullQuote.propTypes = {
  quote: PropTypes.string.isRequired,
  author: PropTypes.string
}
PullQuote.defaultProps = {
  author: null
}
