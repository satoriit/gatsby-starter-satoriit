import PropTypes from "prop-types"
import React, { useRef, useState } from "react"
import { Button, Overlay, Tooltip } from "react-bootstrap"
import { CopyToClipboard } from "react-copy-to-clipboard"
import {
  EmailShareButton,
  FacebookShareButton,
  TwitterShareButton
} from "react-share"

import FacebookIcon from "../../images/social-icon-facebook.inline.svg"
import LinkIcon from "../../images/social-icon-link-outline.inline.svg"
import EnvelopeIcon from "../../images/social-icon-mail.inline.svg"
import TwitterIcon from "../../images/social-icon-twitter.inline.svg"

import {
  horizontalClass,
  rotateIconClass,
  shareBtnClass,
  verticalClass
} from "./Share.module.scss"

// import EnvelopeIcon from "../../images/social-icon-mail.inline.svg";

const Share = ({ className, link, copyLinkBtnID, isVertical }) => {
  const directionClass = isVertical
    ? `${className} ${verticalClass}`
    : `${className} ${horizontalClass}`

  let url
  if (typeof window !== "undefined") {
    url = link || window.location.href
  }

  const [showMessage, setShowMessage] = useState(false)
  const copyTarget = useRef(null)

  if (showMessage === true) {
    const linkButton = document.getElementById(copyLinkBtnID)

    linkButton.addEventListener("mousemove", () => {
      setShowMessage(false)
    })
  }

  return (
    <aside className={directionClass}>
      {!isVertical && <span className="fw-bold me-2">Share this</span>}

      <CopyToClipboard text={url} onCopy={() => setShowMessage(true)}>
        <Button
          id={copyLinkBtnID}
          ref={copyTarget}
          variant="outline-primary"
          className={`${shareBtnClass} ${rotateIconClass}`}>
          <LinkIcon />
        </Button>
      </CopyToClipboard>

      <Overlay target={copyTarget.current} show={showMessage} placement="top">
        {props => (
          // eslint-disable-next-line react/jsx-props-no-spreading
          <Tooltip {...props}>Link copied!</Tooltip>
        )}
      </Overlay>

      <span className={shareBtnClass}>
        <EmailShareButton url={url} className="share d-flex">
          <EnvelopeIcon />
        </EmailShareButton>
      </span>

      <span className={shareBtnClass}>
        <FacebookShareButton url={url} className="d-flex" resetButtonStyle>
          <FacebookIcon />
        </FacebookShareButton>
      </span>

      <span className={shareBtnClass}>
        <TwitterShareButton url={url} className="d-flex">
          <TwitterIcon />
        </TwitterShareButton>
      </span>
    </aside>
  )
}

export default Share

Share.propTypes = {
  className: PropTypes.string,
  link: PropTypes.string,
  copyLinkBtnID: PropTypes.string.isRequired,
  isVertical: PropTypes.bool
}
Share.defaultProps = {
  className: null,
  link: null,
  isVertical: false
}
