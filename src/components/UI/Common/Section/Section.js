import * as PropTypes from "prop-types"
import React from "react"

const Section = ({ padding, children }) => {
  const { top, bottom } = padding
  const className = `${top.mobile} ${top.tablet} ${top.desktop} ${bottom.mobile} ${bottom.tablet} ${bottom.desktop}`
  return <section className={className}>{children}</section>
}

Section.propTypes = {
  children: PropTypes.element,
  padding: PropTypes.shape({
    bottom: PropTypes.shape({
      mobile: PropTypes.string.isRequired,
      tablet: PropTypes.string.isRequired,
      desktop: PropTypes.string.isRequired
    }),
    top: PropTypes.shape({
      mobile: PropTypes.string.isRequired,
      tablet: PropTypes.string.isRequired,
      desktop: PropTypes.string.isRequired
    })
  }).isRequired
}

Section.defaultProps = {
  children: null
}

export default Section
