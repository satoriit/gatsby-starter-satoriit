import { Link } from "gatsby"
import * as PropTypes from "prop-types"
import React from "react"

import * as styles from "./LinkButton.module.scss"

const LinkButton = ({ title, target }) => (
  <Link to={target} className={styles.button}>
    {title}
  </Link>
)

LinkButton.propTypes = {
  title: PropTypes.string.isRequired,
  target: PropTypes.string.isRequired
}

export default LinkButton
