import parseHTML from "html-react-parser"
import PropTypes from "prop-types"
import React from "react"

import { cleanWYSIWYGtext } from "../../../../utils"

import { textBlockClass, darkMode, lightMode } from "./TextBlock.module.scss"

const TextBlock = ({ text, className, alignment, mode }) => {
  if (!text) {
    return null
  }

  const cleanedText = cleanWYSIWYGtext(text)
  const textStyles = `${textBlockClass} ${className} ${
    alignment && alignment.match("center") ? "text-center" : ""
  } ${mode === "dark" ? darkMode : lightMode}
  `

  return <div className={textStyles}>{parseHTML(cleanedText)}</div>
}

TextBlock.propTypes = {
  alignment: PropTypes.oneOf(["start", "end", "center"]),
  className: PropTypes.string,
  text: PropTypes.string.isRequired
}
TextBlock.defaultProps = {
  alignment: "start",
  className: ""
}

export default TextBlock
