import * as PropTypes from "prop-types"
import React from "react"

import DecoratedLine from "./DecoratedLine"
import { lightMode, darkMode } from "./SectionHeading.module.scss"

const SectionHeading = ({ showUnderline, text, alignment, mode }) => (
  <div className={`d-flex flex-column align-items-${alignment}`}>
    <h2 className={`py-3 ${mode === "dark" ? darkMode : lightMode}`}>{text}</h2>
    {showUnderline && <DecoratedLine marginTop="10px" marginBottom="30px" />}
  </div>
)

SectionHeading.propTypes = {
  showUnderline: PropTypes.bool,
  text: PropTypes.string.isRequired,
  alignment: PropTypes.oneOf([
    "start",
    "end",
    "center",
    "between",
    "around",
    "evenly"
  ]),
  mode: PropTypes.oneOf(["light", "dark"])
}

SectionHeading.defaultProps = {
  showUnderline: true,
  alignment: "start",
  mode: "light"
}

export default SectionHeading
