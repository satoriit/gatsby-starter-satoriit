import PropTypes from "prop-types"
import React from "react"

import Iframe from "../Iframe"

import { videoClass } from "./Video.module.scss"

/**
 * Supports YouTube and Vimeo src links.
 */

const Video = ({ title, src, className, frameBorder, allow }) => {
  let Src
  let match
  const yt = /[=\\/]([0-9A-Za-z_-]{10}[048AEIMQUYcgkosw])/i
  const vimeo = /vimeo.*?\/(\d{6,})/i

  if (src) {
    match = src.match(vimeo) || src.match(yt)
  }

  if (!match) {
    // pass
  } else if (match[0].match(yt)) {
    Src = `https://www.youtube.com/embed/${src.match(match[1])}`
  } else if (src.match(vimeo)) {
    Src = `https://player.vimeo.com/video/${src.match(match[1])}`
  }

  return (
    <div className={`${videoClass} ${className}`}>
      <Iframe
        title={title}
        src={Src}
        frameBorder={frameBorder}
        allow={allow}
        allowfullscreen
      />
    </div>
  )
}

export default Video

Video.propTypes = {
  title: PropTypes.string,
  src: PropTypes.string.isRequired,
  className: PropTypes.string,
  frameBorder: PropTypes.number,
  allow: PropTypes.string
}

Video.defaultProps = {
  title: null,
  className: null,
  frameBorder: 0,
  allow: null
}
