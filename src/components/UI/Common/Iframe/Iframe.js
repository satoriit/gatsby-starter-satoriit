/* eslint-disable react/jsx-props-no-spreading */
import PropTypes from "prop-types"
import React from "react"

const Iframe = ({ title, src, className, frameBorder, allow, props }) => (
  <iframe
    className={`${className} position-relative w-100 h-100`}
    title={title}
    src={src}
    frameBorder={frameBorder}
    allow={allow}
    {...props}
  />
)

export default Iframe

Iframe.propTypes = {
  title: PropTypes.string,
  src: PropTypes.string.isRequired,
  className: PropTypes.string,
  frameBorder: PropTypes.number,
  allow: PropTypes.string
}

Iframe.defaultProps = {
  title: null,
  className: null,
  frameBorder: 0,
  /**
   * eg: 'autoplay; fullscreen'
   */
  allow: null
}
