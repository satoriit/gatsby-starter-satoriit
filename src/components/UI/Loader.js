import React, { Fragment } from "react"

import { centerClass, ldsRingClass } from "./Loader.module.scss"

export const Loader = ({ center }) => (
  <div className={`${ldsRingClass} ${center ? centerClass : ""}`}>
    <div />
    <div />
    <div />
    <div />
  </div>
)

export const BtnLoader = () => (
  <>
    &nbsp;
    <span
      className="spinner-border spinner-border-sm"
      role="status"
      aria-hidden="true"
    />
    <span className="sr-only">Loading...</span>
  </>
)
