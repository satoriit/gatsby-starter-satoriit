import PropTypes from "prop-types"
import React from "react"

const DecoratedLine = ({
  color,
  length,
  thinkness,
  marginTop,
  marginBottom
}) => (
  <div
    style={{
      width: length,
      height: thinkness,
      marginTop,
      marginBottom,
      backgroundColor: color
    }}
  />
)

export default DecoratedLine

DecoratedLine.propTypes = {
  /**
   * Line length in px
   */
  length: PropTypes.string,
  /**
   * Line width in px
   */
  thinkness: PropTypes.string,
  marginTop: PropTypes.string,
  marginBottom: PropTypes.string,
  color: PropTypes.string
}

DecoratedLine.defaultProps = {
  length: "88px",
  thinkness: "6px",
  marginTop: "35px",
  marginBottom: "60px",
  color: "#ff835f"
}
