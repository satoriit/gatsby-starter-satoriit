import { Link } from "gatsby"
import { GatsbyImage, getImage } from "gatsby-plugin-image"
import React from "react"
import Col from "react-bootstrap/Col"
import Container from "react-bootstrap/Container"
import Row from "react-bootstrap/Row"

import { createLocalLink } from "../../utils"

const TwoImageBoxesWithLinks = ({
  heading,
  img1,
  link1,
  linkLable1,
  img2,
  link2,
  linkLable2
}) => {
  const image1 = getImage(img1.localFile)
  const image2 = getImage(img2.localFile)

  return (
    <section className="mb-5">
      <Container>
        <h2 className="pb-3">{heading}</h2>
        <Row>
          <Col xs={12} lg={6} className="mb-7 mb-lg-0">
            <GatsbyImage
              className="w-100 h-100 mb-2"
              style={{ borderRadius: "25px" }}
              image={image1}
              alt={img1.altText}
            />
            <Link
              className="linkClass"
              style={{ fontSize: "20px" }}
              to={createLocalLink(link1)}>
              {linkLable1}
            </Link>
          </Col>
          <Col xs={12} lg={6}>
            <GatsbyImage
              className="w-100 h-100 mb-2"
              style={{ borderRadius: "25px" }}
              image={image2}
              alt={img2.altText}
            />
            <Link
              className="linkClass"
              style={{ fontSize: "20px" }}
              to={createLocalLink(link2)}>
              {linkLable2}
            </Link>
          </Col>
        </Row>
      </Container>
    </section>
  )
}

export default TwoImageBoxesWithLinks
