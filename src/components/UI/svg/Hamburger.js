import PropTypes from "prop-types"
import React, { forwardRef } from "react"

const Hamburger = forwardRef(({ className, color, size }, ref) => (
  <svg
    ref={ref}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 48 48"
    className={className}
    width={size}
    height={size}>
    <title>Hamburger Icon</title>
    <g fill={color}>
      <path d="M41,14H7a2,2,0,0,1,0-4H41A2,2,0,0,1,41,14Z" />
      <path d="M41,26H7a2,2,0,0,1,0-4H41A2,2,0,0,1,41,26Z" />
      <path d="M41,38H7a2,2,0,0,1,0-4H41A2,2,0,0,1,41,38Z" />
    </g>
  </svg>
))

Hamburger.propTypes = {
  className: PropTypes.string,
  color: PropTypes.string,
  size: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
}

Hamburger.defaultProps = {
  className: null,
  color: "#000",
  size: "38"
}

export default Hamburger
