import PropTypes from "prop-types"
import React, { forwardRef } from "react"

import { primary } from "../../../styles/variableExports.module.scss"

const TempLogo = forwardRef(({ className, color, size }, ref) => (
  <svg
    ref={ref}
    xmlns="http://www.w3.org/2000/svg"
    xmlnsXlink="http://www.w3.org/1999/xlink"
    viewBox="0 0 473 473"
    className={className}
    width={48 * size}
    height={48 * size}>
    <title>{process.env.ORG_NAME} Logo</title>
    <circle fill="#FFC10E" cx="236.966" cy="236.966" r="236.966" />
    <path
      fill="#FFFFFF"
      d="M81.391,237.123c0,85.911,69.649,155.56,155.56,155.56c85.915,0,155.564-69.649,155.564-155.56
	L81.391,237.123L81.391,237.123z"
    />
    <g>
      <path
        fill="#CCCBCB"
        d="M167.728,237.123c0,46.435,0,92.87,0,139.306c6.024,3.001,12.273,5.609,18.709,7.82
		c0-49.043,0-98.083,0-147.126H167.728z"
      />
      <path
        fill="#CCCBCB"
        d="M283.319,237.123c0,49.511,0,99.018,0,148.529c6.432-2.006,12.666-4.453,18.709-7.24
		c0-47.098,0-94.191,0-141.289L283.319,237.123L283.319,237.123z"
      />
    </g>
    <g>
      <path
        fill="#333333"
        d="M219.181,158.793c-1.684-31.255-23.992-53.56-55.243-55.243
		c-31.184-1.68-53.698,26.522-55.243,55.243c-0.651,12.063,18.061,12,18.709,0c2.537-47.09,70.536-47.09,73.069,0
		C201.12,170.793,219.832,170.856,219.181,158.793L219.181,158.793z"
      />
      <path
        fill="#333333"
        d="M353.985,158.793c-1.684-31.255-23.992-53.56-55.243-55.243
		c-31.184-1.68-53.694,26.522-55.243,55.243c-0.651,12.063,18.061,12,18.709,0c2.537-47.09,70.532-47.09,73.069,0
		C335.924,170.793,354.637,170.856,353.985,158.793L353.985,158.793z"
      />
    </g>
  </svg>
))

TempLogo.propTypes = {
  className: PropTypes.string,
  color: PropTypes.string,
  size: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
}

TempLogo.defaultProps = {
  className: null,
  color: primary,
  size: 1
}

export default TempLogo
