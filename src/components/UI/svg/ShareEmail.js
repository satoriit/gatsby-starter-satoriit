import PropTypes from "prop-types"
import React, { forwardRef } from "react"

import {
  primary,
  primaryOffset
} from "../../../styles/variableExports.module.scss"

const ShareEmail = forwardRef(({ className, color1, color2, size }, ref) => (
  <svg
    ref={ref}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 17 13"
    className={className}
    width={17 * size}
    height={13 * size}>
    <title>Share Email</title>
    <g clipPath="url(#social_email_clip1)">
      <path
        d="M11.343 7.067l5.08 3.796v-1.55l-4.084-3.022 4.083-3.179v-1.55L8.45 7.763.477 1.562v1.55l4.082 3.18L.477 9.312v1.55l5.08-3.796 2.892 2.246 2.894-2.246zM14.828.012c.88 0 1.594.693 1.594 1.55v9.302c0 .856-.713 1.55-1.595 1.55H2.072c-.881 0-1.594-.694-1.594-1.55V1.562c0-.854.716-1.55 1.594-1.55h12.756z"
        fill="url(#social_email_linear1)"
      />
    </g>
    <defs>
      <linearGradient
        id="social_email_linear1"
        x1={8.449}
        y1={0.012}
        x2={8.449}
        y2={12.414}
        gradientUnits="userSpaceOnUse">
        <stop stopColor={color1} />
        <stop offset={1} stopColor={color2} />
      </linearGradient>
      <clipPath id="social_email_clip1">
        <path
          fill="#fff"
          transform="translate(.477 .012)"
          d="M0 0h15.945v12.402H0z"
        />
      </clipPath>
    </defs>
  </svg>
))

ShareEmail.propTypes = {
  className: PropTypes.string,
  color1: PropTypes.string,
  color2: PropTypes.string,
  /**
   * size: factor to multiply width and height by.
   */
  size: PropTypes.number
}

ShareEmail.defaultProps = {
  className: null,
  color1: primary,
  color2: primaryOffset,
  size: 1
}

export default ShareEmail
