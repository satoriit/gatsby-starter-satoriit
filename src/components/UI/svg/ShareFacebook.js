import PropTypes from "prop-types"
import React, { forwardRef } from "react"

import {
  primary,
  primaryOffset
} from "../../../styles/variableExports.module.scss"

const ShareFacebook = forwardRef(({ className, color1, color2, size }, ref) => (
  <svg
    ref={ref}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 9 19"
    className={className}
    width={9 * size}
    height={19 * size}>
    <title>Share Facebook</title>
    <g clipPath="url(#social_facebook_clip1)">
      <path
        d="M8.705.785V3.88c-.38 0-.745.012-1.11-.002-1.44-.054-1.653.207-1.586 1.636.016.352.002.703.002 1.088h2.706l-.32 2.956H5.999v8.727H2.461v-8.7H.753c.002-.995.002-1.951.002-2.963h1.699c0-.8-.026-1.544.01-2.286.023-.5.066-1.017.217-1.487C3.05 1.678 3.933 1.053 5.078.93 6.264.802 7.468.83 8.705.785z"
        fill="url(#social_facebook_linear1)"
      />
    </g>
    <defs>
      <linearGradient
        id="social_facebook_linear1"
        x1={4.735}
        y1={0.785}
        x2={4.735}
        y2={18.285}
        gradientUnits="userSpaceOnUse">
        <stop stopColor={color1} />
        <stop offset={1} stopColor={color2} />
      </linearGradient>
      <clipPath id="social_facebook_clip1">
        <path
          fill="#fff"
          transform="translate(.755 .785)"
          d="M0 0h7.961v17.497H0z"
        />
      </clipPath>
    </defs>
  </svg>
))

ShareFacebook.propTypes = {
  className: PropTypes.string,
  color1: PropTypes.string,
  color2: PropTypes.string,
  /**
   * size: factor to multiply width and height by.
   */
  size: PropTypes.number
}

ShareFacebook.defaultProps = {
  className: null,
  color1: primary,
  color2: primaryOffset,
  size: 1
}

export default ShareFacebook
