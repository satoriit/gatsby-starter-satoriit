import PropTypes from "prop-types"
import React, { forwardRef } from "react"

import {
  lightOrange,
  lightOrangeOffset
} from "../../../styles/variableExports.module.scss"

const Eye = forwardRef(({ className, color1, color2, size }, ref) => (
  <svg
    ref={ref}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 99 50"
    className={className}
    width={99 * size}
    height={50 * size}>
    <path
      d="M0 24.717c6.777-7.188 14.368-12.902 22.814-17.506 21.232-11.578 41.42-8.628 60.694 4.663 4.725 3.26 8.948 7.254 13.377 10.944.638.534 1.14 1.237 1.919 2.099-2.105 2.005-4.004 3.983-6.074 5.758-10.207 8.756-21.41 15.593-34.845 18.153-10.162 1.937-19.858.29-29.24-3.69C18.206 40.71 9.176 34.187 1.197 26.16.834 25.802.542 25.379 0 24.717zM49.45 3.479c-11.862-.03-21.324 9.418-21.335 21.3-.01 11.785 9.339 21.275 21.03 21.348 11.892.072 21.41-9.318 21.461-21.169C70.658 13.032 61.278 3.51 49.45 3.48zM31.164 7.44c-9.946 4.008-18.746 9.731-26.447 17.416 7.97 7.591 16.607 13.432 27.034 17.73-9.678-11.593-8.995-24.97-.587-35.146zm62.7 17.781l-.124-1.35c-6.287-2.444-12.923-.48-19.412-1.217v3.732c5.874 0 11.542.045 17.207-.052.783-.017 1.552-.727 2.328-1.113zm-5.982 4.69c-4.23 0-7.992.183-11.728-.058-2.86-.183-3.176 1.374-3.261 3.504 8.967 1.092 10.876.693 14.99-3.446zm-.765-10.693c-4.33-5.4-9.565-3.022-14.571-3.101.515 1.299 1.25 2.919 2.09 2.977 3.98.276 7.987.124 12.48.124zM77.84 37.412c-5.933-1.347-8.522.014-10.307 5.203 3.437-1.733 6.64-3.353 10.307-5.203zM66.905 6.808c3.687 5.738 4.494 6.137 10.259 5.004l-10.26-5.004z"
      fill="url(#eye_linear1)"
    />
    <path
      d="M37.096 25.458c.041-6.733 5.636-12.254 12.41-12.247 6.806.007 12.38 5.755 12.264 12.656-.113 6.73-5.77 12.195-12.541 12.12-6.706-.08-12.174-5.725-12.133-12.53zm21.17.234c.038-4.96-3.774-8.898-8.677-8.963-4.95-.066-8.9 3.734-8.993 8.646-.092 5.073 3.785 9.077 8.804 9.09 4.93.014 8.829-3.848 8.866-8.773z"
      fill="url(#eye_linear2)"
    />
    <defs>
      <linearGradient
        id="eye_linear1"
        x1={-0.001}
        y1={24.821}
        x2={98.8}
        y2={24.821}
        gradientUnits="userSpaceOnUse">
        <stop stopColor={color1} />
        <stop offset={0.999} stopColor={color2} />
      </linearGradient>
      <linearGradient
        id="eye_linear2"
        x1={37.096}
        y1={25.598}
        x2={61.772}
        y2={25.598}
        gradientUnits="userSpaceOnUse">
        <stop stopColor={color1} />
        <stop offset={0.999} stopColor={color2} />
      </linearGradient>
    </defs>
  </svg>
))

Eye.propTypes = {
  className: PropTypes.string,
  color1: PropTypes.string,
  color2: PropTypes.string,
  /**
   * size: factor to multiply width and height by.
   */
  size: PropTypes.number
}

Eye.defaultProps = {
  className: null,
  color1: lightOrange,
  color2: lightOrangeOffset,
  size: 1
}

export default Eye
