import PropTypes from "prop-types"
import React, { forwardRef } from "react"

import {
  primary,
  primaryOffset
} from "../../../styles/variableExports.module.scss"

const ShareTwitter = forwardRef(({ className, color1, color2, size }, ref) => (
  <svg
    ref={ref}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 18 15"
    className={className}
    width={18 * size}
    height={15 * size}>
    <title>Share Twitter</title>
    <g clipPath="url(#social_twitter_clip1)">
      <path
        d="M.88 12.633a9.652 9.652 0 005.166 1.488c5.337 0 9.664-4.303 9.664-9.61 0-.132-.003-.264-.008-.395.193-.15 1.387-1.103 1.68-1.714 0 0-.97.4-1.919.495h-.006s.002 0 .005-.003c.087-.058 1.31-.886 1.475-1.872 0 0-.685.364-1.646.683-.159.052-.325.104-.497.151A3.39 3.39 0 0012.32.79c-1.874 0-3.391 1.51-3.391 3.37 0 .263.03.518.088.764-.262-.01-4.249-.233-6.96-3.499 0 0-1.62 2.2.966 4.463 0 0-.786-.031-1.465-.434 0 0-.249 2.666 2.68 3.347 0 0-.576.217-1.496.063 0 0 .515 2.153 3.117 2.355 0 0-2.058 1.849-4.98 1.415H.88z"
        fill="url(#social_twitter_linear1)"
      />
    </g>
    <defs>
      <linearGradient
        id="social_twitter_linear1"
        x1={9.13}
        y1={0.79}
        x2={9.13}
        y2={14.121}
        gradientUnits="userSpaceOnUse">
        <stop stopColor={color1} />
        <stop offset={1} stopColor={color2} />
      </linearGradient>
      <clipPath id="social_twitter_clip1">
        <path
          fill="#fff"
          transform="translate(.879 .79)"
          d="M0 0h16.502v13.331H0z"
        />
      </clipPath>
    </defs>
  </svg>
))

ShareTwitter.propTypes = {
  className: PropTypes.string,
  color1: PropTypes.string,
  color2: PropTypes.string,
  /**
   * size: factor to multiply width and height by.
   */
  size: PropTypes.number
}

ShareTwitter.defaultProps = {
  className: null,
  color1: primary,
  color2: primaryOffset,
  size: 1
}

export default ShareTwitter
