import PropTypes from "prop-types"
import React, { forwardRef } from "react"

import {
  primary,
  primaryOffset
} from "../../../styles/variableExports.module.scss"

const ShareLink = forwardRef(({ className, color1, color2, size }, ref) => (
  <svg
    ref={ref}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="1 1 15 15"
    className={className}
    width={15 * size}
    height={15 * size}>
    <title>Share Link</title>
    <path
      d="M8.89 12.247c-.551.008-1.038-.301-1.444-.774-.272-.316-.801-.564-.781-.93.016-.31.52-.574.759-.896.324-.438.554-.402.905-.002.627.713.652.691 1.32.003 1.416-1.462 2.827-2.928 4.254-4.378.25-.254.29-.41.01-.67-.474-.441-.928-.91-1.358-1.395-.243-.274-.393-.23-.613.018-.419.474-.873.913-1.294 1.385-.202.225-.346.298-.572.025-.258-.31-.55-.59-.842-.869-.167-.159-.158-.274 0-.436.6-.618 1.17-1.267 1.79-1.858.722-.684 1.777-.665 2.495.03.704.683 1.388 1.39 2.054 2.113.71.77.715 1.886-.012 2.647-1.745 1.827-3.514 3.63-5.276 5.44-.358.37-.803.546-1.396.547z"
      fill="url(#social_link_linear1)"
    />
    <path
      d="M4.734 16.402a1.884 1.884 0 01-1.385-.587c-.668-.678-1.34-1.35-1.994-2.041-.78-.826-.796-1.972-.009-2.78A461.488 461.488 0 016.613 5.68c.76-.755 1.862-.727 2.65 0 .026.024.05.05.076.075 1.16 1.129 1.156 1.125-.021 2.243-.192.182-.307.22-.506.007-.278-.297-.535-.808-.883-.797-.377.012-.63.528-.931.83-1.331 1.332-2.646 2.684-3.984 4.01-.268.265-.28.422 0 .68.476.436.936.894 1.36 1.381.28.324.457.273.711-.016.32-.364.685-.685 1.018-1.037.16-.17.292-.18.434.006.01.014.024.026.037.039.344.365.92.7.968 1.102.051.43-.6.729-.936 1.092-.513.554-1.028 1.124-1.872 1.108z"
      fill="url(#social_link_linear2)"
    />
    <defs>
      <linearGradient
        id="social_link_linear1"
        x1={11.385}
        y1={0.968}
        x2={11.385}
        y2={12.247}
        gradientUnits="userSpaceOnUse">
        <stop stopColor={color1} />
        <stop offset={1} stopColor={color2} />
      </linearGradient>
      <linearGradient
        id="social_link_linear2"
        x1={5.484}
        y1={5.123}
        x2={5.484}
        y2={16.403}
        gradientUnits="userSpaceOnUse">
        <stop stopColor={color1} />
        <stop offset={1} stopColor={color2} />
      </linearGradient>
    </defs>
  </svg>
))

ShareLink.propTypes = {
  className: PropTypes.string,
  color1: PropTypes.string,
  color2: PropTypes.string,
  /**
   * size: factor to multiply width and height by.
   */
  size: PropTypes.number
}

ShareLink.defaultProps = {
  className: null,
  color1: primary,
  color2: primaryOffset,
  size: 1
}

export default ShareLink
