import PropTypes from "prop-types"
import React, { forwardRef } from "react"

import {
  primary,
  primaryOffset
} from "../../../styles/variableExports.module.scss"

const ShareLinkedIn = forwardRef(({ className, color1, color2, size }, ref) => (
  <svg
    ref={ref}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 17 16"
    className={className}
    width={17 * size}
    height={16 * size}>
    <title>Share LinkedIn</title>
    <path
      d="M.712 2.666c0-.488.174-.89.522-1.207.348-.317.8-.476 1.358-.476.547 0 .99.156 1.327.469.348.321.522.74.522 1.258 0 .468-.169.858-.507 1.17-.348.322-.805.483-1.372.483h-.015c-.547 0-.99-.161-1.328-.483a1.606 1.606 0 01-.507-1.214zm.194 12.8V5.694h3.312v9.772H.906zm5.146 0h3.312v-5.457c0-.341.04-.604.119-.79.14-.331.35-.612.634-.84.283-.23.639-.345 1.066-.345 1.114 0 1.671.737 1.671 2.21v5.222h3.311V9.863c0-1.443-.348-2.538-1.044-3.284-.696-.746-1.616-1.12-2.76-1.12-1.282 0-2.281.542-2.997 1.625v.029h-.015l.015-.03v-1.39H6.052c.02.313.03 1.283.03 2.912 0 1.628-.01 3.915-.03 6.86z"
      fill="url(#social_linkedin_linear1)"
    />
    <defs>
      <linearGradient
        id="social_linkedin_linear1"
        x1={8.439}
        y1={0.983}
        x2={8.439}
        y2={15.466}
        gradientUnits="userSpaceOnUse">
        <stop stopColor={color1} />
        <stop offset={1} stopColor={color2} />
      </linearGradient>
    </defs>
  </svg>
))

ShareLinkedIn.propTypes = {
  className: PropTypes.string,
  color1: PropTypes.string,
  color2: PropTypes.string,
  /**
   * size: factor to multiply width and height by.
   */
  size: PropTypes.number
}

ShareLinkedIn.defaultProps = {
  className: null,
  color1: primary,
  color2: primaryOffset,
  size: 1
}

export default ShareLinkedIn
