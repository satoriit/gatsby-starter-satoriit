import PropTypes from "prop-types"
import React, { forwardRef } from "react"

const ChevronRight = forwardRef(({ className, color, size }, ref) => (
  <svg
    ref={ref}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 24 24"
    className={className}
    width={size}
    height={size}>
    <path
      d="M9.8 16.78l4.11-4.11a1 1 0 000-1.41l-4-4"
      fill="none"
      stroke={color}
      strokeLinecap="round"
      strokeLinejoin="bevel"
      strokeWidth={1.5}
    />
  </svg>
))

ChevronRight.propTypes = {
  className: PropTypes.string,
  color: PropTypes.string,
  size: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
}

ChevronRight.defaultProps = {
  className: null,
  color: "#000",
  size: "32"
}

export default ChevronRight
