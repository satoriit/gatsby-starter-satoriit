import PropTypes from "prop-types"
import React, { forwardRef } from "react"

const SwirlBkgd = forwardRef(({ className, color, width, height }, ref) => (
  <svg
    ref={ref}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 1102 962"
    className={className}
    width={width}
    height={height}
    color={color}
    fill={color}>
    <title>Swirl Background</title>
    <defs>
      <filter id="swirlBkgdFilter1">
        <feColorMatrix />
      </filter>
    </defs>
    <g>
      <g fill={color} fillRule="evenodd" opacity=".44">
        <g
          filter="url(#swirlBkgdFilter1)"
          transform="translate(-490 -158) translate(0 212)">
          <g>
            <path
              d="M907.522 480.991c0 229.887-189.007 416.863-421.329 416.863-232.354 0-421.357-186.976-421.357-416.863H0C0 746.22 218.104 962 486.193 962c268.06 0 486.165-215.781 486.165-481.009 0-194.475-159.971-352.73-356.522-352.73-196.602 0-356.54 158.255-356.54 352.73 0 123.784 101.772 224.485 226.897 224.485 125.097 0 226.865-100.701 226.865-224.485 0-53.05-43.602-96.207-97.222-96.207s-97.256 43.157-97.256 96.207c0 17.699-14.534 32.075-32.387 32.075-17.882 0-32.416-14.376-32.416-32.075 0-88.408 72.7-160.317 162.059-160.317 89.358 0 162.058 71.91 162.058 160.317 0 159.142-130.866 288.617-291.7 288.617-160.85 0-291.716-129.475-291.716-288.617 0-229.851 189.021-416.86 421.358-416.86 232.321 0 421.325 187.009 421.325 416.86H1102C1102 215.767 883.896 0 615.836 0 347.746 0 129.67 215.767 129.67 480.991c0 194.511 159.939 352.749 356.522 352.749 196.57 0 356.49-158.238 356.49-352.749 0-123.748-101.769-224.45-226.847-224.45-125.13 0-226.898 100.702-226.898 224.45 0 53.053 43.635 96.207 97.255 96.207 53.624 0 97.223-43.154 97.223-96.207 0-17.677 14.534-32.057 32.42-32.057 17.885 0 32.42 14.38 32.42 32.057 0 88.426-72.701 160.339-162.063 160.339-89.39 0-162.09-71.913-162.09-160.339 0-159.12 130.87-288.581 291.733-288.581 160.816 0 291.686 129.46 291.686 288.581z"
              transform="translate(490 -54)"
            />
          </g>
        </g>
      </g>
    </g>
  </svg>
))

SwirlBkgd.propTypes = {
  className: PropTypes.string,
  color: PropTypes.string,
  width: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  height: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
}

SwirlBkgd.defaultProps = {
  className: null,
  color: "#000",
  width: "1102",
  height: "962"
}

export default SwirlBkgd
