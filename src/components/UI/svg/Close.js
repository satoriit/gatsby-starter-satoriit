import PropTypes from "prop-types"
import React, { forwardRef } from "react"

const Close = forwardRef(({ className, color, size }, ref) => (
  <svg
    ref={ref}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 30 30"
    className={className}
    width={size}
    height={size}>
    <title>Close Icon</title>
    <g fill={color}>
      <g>
        <path
          d="M15 5c.69 0 1.25.56 1.25 1.25v7.5h7.5c.647 0 1.18.492 1.244 1.122L25 15c0 .69-.56 1.25-1.25 1.25h-7.5v7.5c0 .647-.492 1.18-1.122 1.244L15 25c-.69 0-1.25-.56-1.25-1.25v-7.5h-7.5c-.647 0-1.18-.492-1.244-1.122L5 15c0-.69.56-1.25 1.25-1.25h7.5v-7.5c0-.647.492-1.18 1.122-1.244z"
          transform="translate(-1242 -748) translate(1055 181) rotate(-45 792.93 72.771)"
        />
      </g>
    </g>
  </svg>
))

Close.propTypes = {
  className: PropTypes.string,
  color: PropTypes.string,
  size: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
}

Close.defaultProps = {
  className: null,
  color: "#000",
  size: "38"
}

export default Close
