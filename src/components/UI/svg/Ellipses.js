import PropTypes from "prop-types"
import React, { forwardRef } from "react"

const Ellipses = forwardRef(({ className, color, size }, ref) => (
  <svg
    ref={ref}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 342 342"
    className={className}
    width={size}
    height={size}>
    <title>Ellipses Icon</title>
    <g fill={color}>
      <path d="M45.225 125.972C20.284 125.972 0 146.256 0 171.191c0 24.94 20.284 45.219 45.225 45.219 24.926 0 45.219-20.278 45.219-45.219 0-24.935-20.293-45.219-45.219-45.219zM173.409 125.972c-24.938 0-45.225 20.284-45.225 45.219 0 24.94 20.287 45.219 45.225 45.219 24.936 0 45.226-20.278 45.226-45.219 0-24.935-20.29-45.219-45.226-45.219zM297.165 125.972c-24.932 0-45.222 20.284-45.222 45.219 0 24.94 20.29 45.219 45.222 45.219 24.926 0 45.217-20.278 45.217-45.219 0-24.935-20.291-45.219-45.217-45.219z" />
    </g>
  </svg>
))

Ellipses.propTypes = {
  className: PropTypes.string,
  color: PropTypes.string,
  size: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
}

Ellipses.defaultProps = {
  className: null,
  color: "#000",
  size: "16"
}

export default Ellipses
