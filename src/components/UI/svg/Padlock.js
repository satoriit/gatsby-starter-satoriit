import PropTypes from "prop-types"
import React, { forwardRef } from "react"

import {
  lightOrange,
  lightOrangeOffset
} from "../../../styles/variableExports.module.scss"

const Padlock = forwardRef(({ className, color1, color2, size }, ref) => (
  <svg
    ref={ref}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 62 89"
    className={className}
    width={62 * size}
    height={89 * size}>
    <g clipPath="url(#padlock_clip1)" strokeMiterlimit={10}>
      <path
        d="M50.797 28.03v-7.208c0-10.977-8.84-19.404-19.75-19.404s-19.75 8.427-19.75 19.404v7.208c-5.46 0-9.885 4.45-9.885 9.948v39.668c0 5.488 4.425 9.947 9.885 9.947h39.406c5.46 0 9.885-4.45 9.885-9.947V37.968c0-5.46-4.377-9.881-9.79-9.938zm-30.623-7.208c0-8.342 2.956-11.043 11.07-11.043 6.242 0 11.74 2.71 11.74 11.043v7.208h-22.81v-7.208z"
        fill="none"
        stroke="url(#padlock_linear3)"
        strokeWidth={3.2}
      />
      <path
        d="M23.988 71V58.505h-2.383v-2.041h2.383v-4.24c0-5.189 3.507-8.414 8.418-8.414 2.563 0 4.496.755 6.148 2.424l-1.967 1.929c-.978-.981-1.967-1.782-4.192-1.782-3.507 0-5.507 2.233-5.507 6.214v3.869h5.507v2.041h-5.507v9.925h11.666v2.572H23.988v0z"
        fill="url(#padlock_linear1)"
        stroke="url(#padlock_linear2)"
      />
    </g>
    <defs>
      <linearGradient
        id="padlock_linear1"
        x1={21.605}
        y1={57.404}
        x2={38.553}
        y2={57.404}
        gradientUnits="userSpaceOnUse">
        <stop stopColor={color1} />
        <stop offset={0.999} stopColor={color2} />
      </linearGradient>
      <linearGradient
        id="padlock_linear2"
        x1={21.605}
        y1={57.404}
        x2={38.553}
        y2={57.404}
        gradientUnits="userSpaceOnUse">
        <stop stopColor={color1} />
        <stop offset={0.999} stopColor={color2} />
      </linearGradient>
      <linearGradient
        id="padlock_linear3"
        x1={1.412}
        y1={44.503}
        x2={60.587}
        y2={44.503}
        gradientUnits="userSpaceOnUse">
        <stop stopColor={color1} />
        <stop offset={0.999} stopColor={color2} />
      </linearGradient>
      <clipPath id="padlock_clip1">
        <path fill="none" d="M0 0h62v89H0z" />
      </clipPath>
    </defs>
  </svg>
))

Padlock.propTypes = {
  className: PropTypes.string,
  color1: PropTypes.string,
  color2: PropTypes.string,
  /**
   * size: factor to multiply width and height by.
   */
  size: PropTypes.number
}

Padlock.defaultProps = {
  className: null,
  color1: lightOrange,
  color2: lightOrangeOffset,
  size: 1
}

export default Padlock
