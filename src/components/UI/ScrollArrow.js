import React, { useState } from "react"
import { ArrowUpCircle } from "react-bootstrap-icons"

import useScrollPosition from "../../hooks/useScrollPosition"

import { scrollTopClass } from "./ScrollArrow.module.scss"

const ScrollArrow = () => {
  const [hideOnScroll, setHideOnScroll] = useState(false)

  useScrollPosition(
    ({ prevPos, currPos }) => {
      const isShow = currPos.y > prevPos.y && currPos.y < -1800
      if (isShow !== hideOnScroll) {
        setHideOnScroll(isShow)
      }
    },
    [hideOnScroll],
    false,
    false,
    200
  )

  const scrollTop = () => {
    window.scrollTo({ top: 1500, behavior: "smooth" })
  }
  return (
    <div>
      <ArrowUpCircle
        className={scrollTopClass}
        onClick={scrollTop}
        style={{ height: 40, display: hideOnScroll ? " flex" : "none" }}
      />
    </div>
  )
}

export default ScrollArrow
