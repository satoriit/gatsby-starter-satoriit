import { Link as GatsbyLink } from "gatsby"
import PropTypes from "prop-types"
import React from "react"

import { createLocalLink } from "../../utils"
import setIsExternalLink from "../../utils/setIsExternalLink"

const Link = ({ className, title, link, children }) => {
  const isExternal = setIsExternalLink(link)
  const isMailTo = link.match("mailto:")
  const isTel = link.match("tel:")
  const url = link || "/"

  if (isExternal || isMailTo || isTel) {
    return (
      <a
        className={`${className != null && className} link`}
        href={url}
        target="_blank"
        rel="noopener noreferrer">
        {title || children}
      </a>
    )
  }

  return (
    <GatsbyLink className={`${className} link`} to={createLocalLink(url)}>
      {title || children}
    </GatsbyLink>
  )
}

export default Link

Link.propTypes = {
  className: PropTypes.string,
  title: PropTypes.string,
  link: PropTypes.string
}
Link.defaultProps = {
  className: null,
  title: null,
  link: "/"
}
