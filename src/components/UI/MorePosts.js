import { graphql, useStaticQuery } from "gatsby"
import { getImage } from "gatsby-plugin-image"
import React from "react"
import { Row, Col } from "react-bootstrap"

import { Section } from "../Layout"

import Card from "./Card"

const MorePosts = ({ ...props }) => {
  const data = useStaticQuery(graphql`
    {
      allWpPost(
        filter: {
          tags: { nodes: { elemMatch: { slug: { eq: "recommended" } } } }
        }
        limit: 4
      ) {
        nodes {
          uri
          date(formatString: "DD MMM YYYY", locale: "en-GB")
          title
          id
          link
        }
      }
    }
  `)

  const filteredData = data.allWpPost.nodes
    .filter(item => item.uri !== props.uri)
    .slice(0, 3)

  return (
    <Section heading={{ title: props.sectionHeading }} variant="default">
      <Row>
        {filteredData.map(post => {
          const {
            title,
            id,
            link,
            postsLayouts: { postObject }
          } = post

          const { mainImage } = postObject[0]

          const mainImg = getImage(mainImage?.localFile)

          return (
            <Col
              className="d-flex justify-content-center justify-content-lg-start mb-4"
              sm={12}
              lg={4}
              key={id}>
              <Card
                link={link}
                image={mainImg}
                alt={mainImage?.altText}
                title={title}
              />
            </Col>
          )
        })}
      </Row>
    </Section>
  )
}

export default MorePosts
