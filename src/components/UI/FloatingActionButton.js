/* eslint-disable */
/** LINTING HAS BEEN DISTABLED JUST FOR SETUP.
  * PLEASE TURN LINTTING BACK ON WHEN WORKING ON THIS FILE.
  * THANK YOU! <3
  */



import { graphql, useStaticQuery } from "gatsby"
import React from "react"

import {
  mobileLinkClass,
  navLinkClass
} from "../Layout/Navbar/NavbarContainer.module.scss"

import Button from "./Button"
import {
  floatingContainerClass,
  textClass,
  innerContainerClass
} from "./FloatingActionButton.module.scss"
import Link from "./Link"
import { Handshake } from "./svg"

// const FAB_QUERY = graphql`
//   query GET_FAB_PROPS {
//     wpMenu(name: { eq: "Main Menu" }) {
//       floatingActionButton {
//         fabContent {
//           text
//           link {
//             url
//           }
//         }
//       }
//     }
//   }
// `

export const FloatingActionButton = () => {
  // const data = useStaticQuery(FAB_QUERY)
  // const { text, link } = data.wpMenu.floatingActionButton.fabContent
  const [link, text] = ["TEMPORARY VALUE", "TEMPORARY VALUE"]
  return (
    <Link link={link?.url}>
      <div
        className={`d-none d-lg-flex justify-content-end ${floatingContainerClass}`}>
        <div
          className={`${innerContainerClass} d-flex flex-column  align-items-center justify-content-center `}>
          <Handshake />
          <p className={textClass}>{text}</p>
        </div>
      </div>
    </Link>
  )
}

export const FloatingActionButtonForMobile = () => {
  // const data = useStaticQuery(FAB_QUERY)
  const { text, link } = data.wpMenu.floatingActionButton.fabContent
  return (
    <Button
      className={`${navLinkClass} ${mobileLinkClass} lead`}
      onClick={link?.url}
      title={text}
      variant="primary"
    />
  )
}
