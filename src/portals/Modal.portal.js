/* eslint-disable prettier/prettier */
import React from "react"
import { Modal } from "react-bootstrap"
import ReactDOM from "react-dom"
import "../styles/bootstrap/_modal.scss"

const MODAL_OPEN = true
const portalRoot =
  typeof document !== `undefined`
    ? document.getElementById("portal-root")
    : null

export const ModalPortal = ({
  hide,
  heading,
  hideCloseButton,
  children,
  mode,
}) => {
  const { name } = children.type
  // StoreLocatorModal
  // AddedToCartModal
  // ProductNotificationModal
  // BikeAssemblyModal

  const modalType =
    name ||
    (children.type.displayName &&
      children.type.displayName.match(/\((.*)\)/).pop())

  return ReactDOM.createPortal(
    <Modal
      className="py-5"
      show={MODAL_OPEN}
      onHide={hide}
      contentClassName={mode || modalType}
    >
      {hideCloseButton ? (
        <Modal.Header>
          {heading && <Modal.Title>{heading}</Modal.Title>}
        </Modal.Header>
      ) : (
        <Modal.Header closeButton>
          {heading && <Modal.Title>{heading}</Modal.Title>}
        </Modal.Header>
      )}

      <Modal.Body>{children}</Modal.Body>
    </Modal>,
    portalRoot
  )
}
export const ToggleModal = ({ toggle, content, onHide }) => {
  const [isShown, setIsShown] = React.useState(false)
  const hide = () => {
    // eslint-disable-next-line no-unused-expressions
    onHide && onHide()
    setIsShown(false)
  }
  const show = () => setIsShown(true)

  return (
    <>
      {toggle(show)}
      {isShown && content(hide)}
    </>
  )
}
