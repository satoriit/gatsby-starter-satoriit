const textHighlight = (question, searchText) => {
  let begginingText = ""
  let endText = ""
  const startPosition = question.toLowerCase().indexOf(searchText.toLowerCase())

  if (startPosition >= 0 && searchText) {
    const endPosition = startPosition + searchText.length
    if (startPosition > 0) {
      begginingText = question.substring(0, startPosition)
    }
    if (endPosition <= question.length)
      endText = question.substring(endPosition, question.length)
  }
  return `${begginingText}<strong>${searchText}</strong>${endText}`
}

export default textHighlight
