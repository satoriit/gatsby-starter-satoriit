import titleCase from "./titleCase"

export const cleanCrumbPath = crumbs =>
  crumbs.map(item => {
    const newLable = item.crumbLabel.replace("-", " ")
    return { pathname: item.pathname, crumbLabel: titleCase(newLable) }
  })

export const cleanCrumbLabel = label => titleCase(label.replace("-", " "))
