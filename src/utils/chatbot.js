const openChatbot = () => {
  if (window.BE_API && window.BE_API.openChatWindow) {
    window.BE_API.openChatWindow()
  }
}

export default openChatbot
