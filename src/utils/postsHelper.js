export const filterPostsByCategories = (allPosts, categories) => {
  if (categories === null || categories === "" || categories === undefined) {
    return allPosts
  }

  const posts = []
  const categoryFilters = categories.map(category => category.name)

  allPosts.forEach(post => {
    const matchedCategories = post.categories.nodes.filter(
      category => categoryFilters.indexOf(category.name) !== -1
    )

    if (matchedCategories.length) {
      posts.push(post)
    }
  })

  return posts.sort((a, b) => new Date(b.date) - new Date(a.date))
}

function immutableMove(arr, from, to) {
  return arr.reduce((prev, current, idx, self) => {
    if (from === to) {
      prev.push(current)
    }
    if (idx === from) {
      return prev
    }
    if (from < to) {
      prev.push(current)
    }
    if (idx === to) {
      prev.push(self[from])
    }
    if (from > to) {
      prev.push(current)
    }
    return prev
  }, [])
}

export const prioritisePosts = posts => {
  const originalPosts = posts
  if (posts.length > 3) {
    const indexes = posts
      .map((post, i) =>
        post.tags.nodes.filter(tag => tag.id === process.env.FEATURED_TAG_ID)
          .length > 0
          ? i
          : undefined
      )
      .filter(x => x)
    for (let i = 0; i < indexes.length; i += 1) {
      // eslint-disable-next-line no-param-reassign
      posts = immutableMove(originalPosts, indexes[i], i)
    }
  }

  return posts
}
