const setIsExternalLink = url => {
  const regex = /http.{0,2}\/\/.*?(?=\/)/
  const linkMatch = url.match(regex)
  if (linkMatch && process.env.SITE_URL.indexOf(linkMatch[0]) === -1) {
    return true
  }
  return false
}

export default setIsExternalLink
