const cleanWYSIWYGtext = text => {
  let cleanedText

  if (text) {
    const linkRegex = new RegExp(process.env.SITE_URL, "g")
    cleanedText = text.replace(/&nbsp;/g, "")
    cleanedText = cleanedText.replace(/<div/g, "<p")
    cleanedText = cleanedText.replace(/<\/div/g, "</p")
    cleanedText = cleanedText.replace(/<b>/g, "<strong>")
    cleanedText = cleanedText.replace(/<\/b>/g, "</strong>")
    cleanedText = cleanedText.replace(linkRegex, "")
  }
  return cleanedText || text
}

export default cleanWYSIWYGtext
