const createLocalLink = url => {
  if (url === null) {
    return null
  }
  if (url === `#`) {
    return null
  }
  if (url === `${process.env.SITE_URL}/home/`) {
    return "/"
  }
  if (url === `/home/`) {
    return "/"
  }
  return url.replace(process.env.SITE_URL, ``)
}

export default createLocalLink
