const validateForm = form => {
  const emailRegex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/i

  const emailTest = emailRegex.test(form.elements.email.value)

  const formCheck = form.checkValidity()

  if (emailTest && formCheck) {
    return true
  }
  return false
}

export default validateForm
