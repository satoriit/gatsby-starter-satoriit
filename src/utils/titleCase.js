const titleCase = str => {
  const strLowerCase = str.toLowerCase()
  const wordArr = strLowerCase.split(" ").map(currentValue => {
    if (currentValue[0]) {
      return currentValue[0].toUpperCase() + currentValue.substring(1)
    }
    return currentValue
  })

  return wordArr.join(" ")
}

export default titleCase
