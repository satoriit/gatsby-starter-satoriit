/* eslint-disable no-unused-vars */
/* eslint-disable unused-imports/no-unused-imports */
/* eslint-disable react/jsx-props-no-spreading */

import { graphql } from "gatsby"
import React from "react"
import { v4 as uuidv4 } from "uuid"

import {
  // Please keep alphabetically sorted for comparison with page.js
  PlainText
} from "../components/AcfPageBlocks"
import Layout from "../components/Layout/Layout"

const Post = ({ location, history, data, pageContext }) => {
  const {
    title,
    uri,
    date,
    author: {
      node: { name }
    },
    flexLayouts: { pageLayouts },
    seo: { seoComponents }
  } = data.wpPost

  let seoComponent

  if (seoComponents?.length) {
    seoComponents.forEach(component => {
      switch (component.__typename) {
        case "WpPost_Seo_SeoComponents_SeoComponents": {
          seoComponent = component
          // console.log("posts build SEO", seoComponent)
          break
        }
        default:
        // console.log(component.__typename + " for SEO?")
      }
    })
  }

  if (
    !seoComponent ||
    seoComponent.seoTitle === null ||
    seoComponent.seoTitle === ""
  ) {
    seoComponent = { seoTitle: title }
  }

  return (
    <Layout seoComponent={seoComponent} location={location}>
      {pageLayouts?.length > 0 &&
        pageLayouts.map(block => {
          switch (block.__typename) {
            case "WpPost_Flexlayouts_PageLayouts_PlainText":
              return <PlainText key={uuidv4()} {...block} />
            default:
              // console.log("This is the End!", block.__typename)
              return "" // this should not happen
          }
        })}
    </Layout>
  )
}

export default Post

export const postQuery = graphql`
  query GET_POST($id: String!) {
    wpPost(id: { eq: $id }) {
      title
      content
      uri
      date(formatString: "DD MMM YYYY", locale: "en-GB")
      author {
        node {
          name
        }
      }
      flexLayouts {
        pageLayouts {
          __typename
          ...PlainTextPostFragment
        }
      }
      seo {
        seoComponents {
          __typename
          ...postMetadataSectionFragment
        }
      }
    }
  }
`
