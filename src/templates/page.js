/* eslint-disable react/jsx-props-no-spreading */
import { graphql } from "gatsby"
import parseHTML from "html-react-parser"
import React from "react"
import { v4 as uuidv4 } from "uuid"

import { PlainText } from "../components/AcfPageBlocks"
import Layout from "../components/Layout/Layout"

// import { MetadataSection } from "../components/AcfSeoBlocks"

const Page = ({ location, data, crumbLabel, history }) => {
  const {
    title,
    content,
    flexLayouts: { pageLayouts },
    seo: { seoComponents }
  } = data.wpPage

  // const { categoryPath, size, id, slugs, history } = pageContext
  //
  // const breadcrumbData = [ ...history, {name: pageContext.name, path: categoryPath}]

  let seoComponent

  if (seoComponents?.length) {
    seoComponents.map(component => {
      switch (component.__typename) {
        case "WpPage_Seo_SeoComponents_SeoComponents": {
          seoComponent = component
          break
        }
        default:
      }
      return ""
    })
  }

  if (
    !seoComponent ||
    seoComponent.seoTitle === null ||
    seoComponent.seoTitle === ""
  ) {
    seoComponent = { seoTitle: title }
  }

  return (
    <>
      <Layout seoComponent={seoComponent} location={location}>
        {/* <Breadcrumb data={breadcrumbData}/> */}
        <main>
          {content && <div className={title}>{parseHTML(content)}</div>}
          {pageLayouts?.length &&
            pageLayouts.map(block => {
              switch (block.__typename) {
                case "WpPage_Flexlayouts_PageLayouts_PlainText":
                  return <PlainText key={uuidv4()} {...block} />
                default:
                  return ""
              }
            })}
        </main>
      </Layout>
    </>
  )
}

export default Page

export const pageQuery = graphql`
  query GET_PAGE($id: String!) {
    wpPage(id: { eq: $id }) {
      title
      content
      uri
      date
      flexLayouts {
        pageLayouts {
          __typename
          ...PlainTextPageFragment
        }
      }
      seo {
        seoComponents {
          __typename
          ...metadataSectionFragment
        }
      }
    }
  }
`
